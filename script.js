"use strict"

class Employee {
  constructor(name, age, salary) {
    this.name = name;
    this.age = age;
    this.salary = salary;
  }

  get getName() {
    return this.name;
  }

  set setName(newName) {
    this.name = newName;
  }

  get getAge() {
    return this.age;
  }

  set setAge(newAge) {
    this.age = newAge;
  }

  get getSalary() {
    return this.salary;
  }

  set setSalary(newSalary) {
    this.salary = newSalary;
  }
}

class Programmer extends Employee {
  constructor(name, age, salary, lang) {
    super(name, age, salary);
    this.lang = lang;
  }

  get getSalary() {
    return this.salary * 3;
  }
}

const programmer1 = new Programmer("Max", 30, 5000, "Python");
const programmer2 = new Programmer("Kos", 20, 1000, "C++");
const programmer3 = new Programmer("Stas", 25, 2000, "Java Script");
const programmer4 = new Programmer("Taras", 55, 3000, "C#");

console.log(programmer1);
console.log(programmer2);
console.log(programmer3);
console.log(programmer4);

